#makefile
CFLAGS=-std=gnu99 -g -Wall
 
all: mftp mftpserve


mftp: mftp.h
	gcc $(CFLAGS) mftp.c -o mftp

mftpserve: mftp.h
	gcc $(CFLAGS) mftpserve.c -o mftpserve

clean:
	rm -f mftp mftpserve *.o *.gch
