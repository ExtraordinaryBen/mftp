#ifndef MFTP_H
#define MFTP_H

#define PGRM_NAME "MFTP"
#define MSG_TERM "\n"

#define MY_PORT_NUMBER 49999
#define MSG_BUF PATH_MAX + 3
#define MAX_PENDING 4

#define FD_NULL -100
#define USE_DATA_FD -500

#define CMD_DATA 'D'
#define CMD_CD   'C'
#define CMD_LIST 'L'
#define CMD_GET  'G'
#define CMD_PUT  'P'
#define CMD_QUIT 'Q'

#define SRV_ACK  'A'
#define SRV_ERR  'E'

#define STD_IN 0
#define STD_OUT 1

#include <sys/types.h>
#include <sys/stat.h>
#include <fcntl.h>
#include <sys/wait.h>
#include <sys/socket.h>
#include <netinet/in.h>
#include <arpa/inet.h>
#include <netdb.h>
#include <stdlib.h>
#include <string.h>
#include <errno.h>
#include <stdio.h>
#include <unistd.h>
#include <time.h>
#include <limits.h>
#include <stdbool.h>
#include <libgen.h>
#include <assert.h>

uint16_t dataPort = 0;
int datafd = -1;
int dataClientFD = -1;

bool file_exists(char *file)
{
	struct stat status;
  int result = stat(file, &status);
  return (result == 0);
}

int open_path(char *pathname)
{
	int fd = open(pathname, (O_CREAT | O_RDWR), (S_IRUSR | S_IWUSR | S_IRGRP | S_IROTH));
	if(fd < 0) {
		perror("open error");
		exit(0);
	}
	return fd;
}

void close_dataPort()
{
	close(dataClientFD);
	dataClientFD = -1;
	close(datafd);
	datafd = -1;
	dataPort = -1;
}

void IO_redir(int inputFD, char *cmd, char *arg, int outputFD)
{
	int pid;
	if((pid = fork())) {
		waitpid(pid, NULL, 0);
		close_dataPort();
		close(inputFD);
		close(outputFD);
	}
	else {
		if(inputFD != STD_IN && inputFD != FD_NULL)
			dup2(inputFD, STD_IN);
		if(outputFD != FD_NULL) {
			if(outputFD < 0) {
				perror("get error");
				exit(0);
			}
			else
				dup2(outputFD, STD_OUT);
		}
		execlp(cmd, cmd, arg, (char *)NULL);
		
		perror("IO_redir fork");
		close_dataPort();
		close(inputFD);
		close(outputFD);
		exit(0);
	}
}

#endif
