/* Program:     mftp
 * Author:      Ben DeCamp
 * Email:       benjamin.decamp@wsu.edu
 * Date:        2017-05-01
 * 
 * Description: A minature version of an FTP Client    
 * 
 */

#include "mftp.h"

#define CLIENT_EXIT "exit"
#define CLIENT_CD   "cd"
#define CLIENT_RCD  "rcd"
#define CLIENT_LS   "ls"
#define CLIENT_RLS  "rls"
#define CLIENT_GET  "get"
#define CLIENT_SHOW "show"
#define CLIENT_PUT  "put"

#define NUM_OF_CMDS 8

#define CMD_DELIM   "\f\r\t\v \n"

char commands[][NUM_OF_CMDS] = { CLIENT_EXIT, CLIENT_CD, CLIENT_RCD,
         CLIENT_LS, CLIENT_RLS, CLIENT_GET, CLIENT_SHOW, CLIENT_PUT };

char address[HOST_NAME_MAX];
char cmdStr[MSG_BUF];
unsigned int msgLength = 0;
char response[MSG_BUF];

int init_socket_client(struct sockaddr_in* servAddr, int port)
{
	int socketfd = socket( AF_INET, SOCK_STREAM, 0);
	memset( servAddr, 0, sizeof(*servAddr));
	servAddr->sin_family = AF_INET;
	servAddr->sin_port = htons(port);
	
	return socketfd;
}

void init_sockAddr_client(struct sockaddr_in* servAddr, struct hostent* hostEntry, struct in_addr** pptr)
{	
	hostEntry = gethostbyname(address);
	if(hostEntry == NULL) {
		herror("HostName");
		exit(1);
	}
	pptr = (struct in_addr **) hostEntry->h_addr_list;
	memcpy(&(servAddr->sin_addr), *pptr, sizeof(struct in_addr));
}

bool valid_cmd(char *cmd)
{
	for(int i = 0; i < NUM_OF_CMDS; i++) {
		if(cmd == NULL)
			break;
		if(strcmp(cmd, commands[i]) == 0)
			return true;
	}
	printf("%s: Invalid command! (%s)\n", PGRM_NAME, cmd);
	return false;
}

void client_cd(char *path)
{
	if(chdir(path) < 0)
		perror("client_cd error");
}

void client_ls()
{
	if(fork()) {
		wait(NULL);
	}
	else {
		int fd[2];
		assert( pipe(fd) >= 0);
		int rdr = fd[0], wtr = fd[1];
		
		if(fork()) {	//parent			
			close(wtr);
			dup2(rdr, STD_IN);
			close (rdr);
			execlp("more", "more", "-l", (char *)NULL);
			fprintf(stderr, "Could not run `more`\n");
		}
		else { //child
			close(rdr);
			dup2(wtr, STD_OUT);
			close (wtr);
			execlp("ls", "ls", "-l", (char *)NULL);
			fprintf(stderr, "Could not run `ls`\n");
		}
		
	}
	
}

int wait_ack(int fd)
{
	int readStat = 0;
	memset(response, 0, MSG_BUF);
	char *byte = response;
	
	while( (readStat = read(fd, byte++, 1)) > 0 ) {
		if( *(byte-1) == '\n')
			break;
	}
	
	if(readStat < 1) {
		perror("wait_ack error");
		return -1;
	}
	
	//printf("Response: %s", response);
	
	if(response[0] == 'E') {
		fprintf(stderr, "%s", response+1);
		return -1;
	}
		
	if(response[0] == 'A') {
		//printf("Server acknowledged\n");
		if(response[1] != '\n') {
			char *str = strtok(response, "A\n");
			return strtol(str, NULL, 10); //parse data port
		}	
	}
	return 0;
}

void send_cmd(int fd, char *cmd, char* arg)
{
	write(fd, cmd, 1);
	
	if(arg != NULL)
		write(fd, arg, strlen(arg));
		
	write(fd, "\n", 1);
}

void client_IO_redir(int cmdFD, int inputFD, char *cmd, char *path, char *ioCmd, char *ioArg, int outputFD)
{
	if(dataPort > 0) {
		struct sockaddr_in servAddr;
		datafd = init_socket_client(&servAddr, dataPort);
		
		struct hostent* hostEntry = NULL;
		struct in_addr **pptr = NULL;
		
		init_sockAddr_client(&servAddr, hostEntry, pptr);
		
		while(connect(datafd, (struct sockaddr *) &servAddr, sizeof(servAddr)) < 0)
		{
			perror("datafd connect");
			sleep(3);
		}

		send_cmd(cmdFD, cmd, path);

		if(wait_ack(cmdFD) > -1) {
			int inFD = (inputFD == USE_DATA_FD ? datafd : inputFD);
			int outFD = (outputFD == USE_DATA_FD ? datafd : outputFD);
			IO_redir(inFD, ioCmd, ioArg, outFD);
		}
		
	}
	else {
		fprintf(stderr, "Error: Data connection has not been established!\n");
	}
}

void cmd_loop(int fd)
{
	bool exit = false;
	char *str = NULL;
	
	do {
		memset(cmdStr, 0, MSG_BUF);
		do {
			printf("%s:> ", PGRM_NAME);
			
			fgets(cmdStr, MSG_BUF, stdin);
			str = strtok(cmdStr, CMD_DELIM);
			
		} while(!valid_cmd(str));
		
		
		if(strcmp(str, CLIENT_EXIT) == 0) {
			exit = true;
		}
		else {
			//Process command
			if(strcmp(str, CLIENT_CD) == 0) {
				str = strtok(NULL, MSG_TERM);
				printf("cd: %s\n", str);
				client_cd(str);
			}
			if(strcmp(str, CLIENT_RCD) == 0) {
				str = strtok(NULL, MSG_TERM);
				printf("rcd: %s\n", str);
				send_cmd(fd, "C", str);
				wait_ack(fd);
			}
			else if(strcmp(str, CLIENT_LS) == 0) {
				client_ls();
			}
			else if(strcmp(str, CLIENT_RLS) == 0) {
				//createDataConnection
				send_cmd(fd, "D", NULL);
				dataPort = wait_ack(fd);
				printf("dataPort: %d\n", dataPort);
				
				client_IO_redir(fd, USE_DATA_FD, "L", NULL, "more", "-20", FD_NULL);
				
			}
			else if(strcmp(str, CLIENT_GET) == 0) {
				str = strtok(NULL, MSG_TERM);
				//createDataConnection
				send_cmd(fd, "D", NULL);
				dataPort = wait_ack(fd);
				printf("dataPort: %d\n", dataPort);
				
				client_IO_redir(fd, USE_DATA_FD, "G", str, "cat", (char *)NULL, open_path(basename(str)));
			}
			else if(strcmp(str, CLIENT_SHOW) == 0) {
				str = strtok(NULL, MSG_TERM);
				//createDataConnection
				send_cmd(fd, "D", NULL);
				dataPort = wait_ack(fd);
				printf("dataPort: %d\n", dataPort);
				
				client_IO_redir(fd, USE_DATA_FD, "G", str, "more", "-20", FD_NULL);
			}
			else if(strcmp(str, CLIENT_PUT) == 0) {
				str = strtok(NULL, MSG_TERM);
				//createDataConnection
				send_cmd(fd, "D", NULL);
				dataPort = wait_ack(fd);

				client_IO_redir(fd, open_path(str), "P", basename(str), "cat", (char *)NULL, USE_DATA_FD);
			}
			
		}

	
 } while(!exit);
 
 send_cmd(fd, "Q", NULL);
 //wait for response
 wait_ack(fd);
}

int main(int argc, char **argv)
{
	
	if(argc == 1) {
		printf("Enter host address: ");
		fgets(address, HOST_NAME_MAX, stdin);
		char *tmp = address;
		while (tmp != NULL) {
			if( *tmp == '\n') {
				*tmp = '\0';
				break;
			}
			tmp++;
		}
	}
	else {
		strcpy(address, argv[1]);
	}
	
	struct sockaddr_in servAddr;
	int socketfd = init_socket_client(&servAddr, MY_PORT_NUMBER);
	
	struct hostent* hostEntry = NULL;
	struct in_addr **pptr = NULL;
	
	init_sockAddr_client(&servAddr, hostEntry, pptr);
	
	if(connect(socketfd, (struct sockaddr *) &servAddr, sizeof(servAddr)) < 0)
	{
		perror("socketfd connect");
		exit(1);
	}
		
	cmd_loop(socketfd);
	
	close(socketfd);	 
	return 0;
}

