/* Program:     mftpserve
 * Author:      Ben DeCamp
 * Email:       benjamin.decamp@wsu.edu
 * Date:        2017-05-01
 * 
 * Description: A minature version of an FTP Server    
 * 
 */

#include "mftp.h"

pid_t pid;
char msg[MSG_BUF];

int init_socket()
{
	int fd = socket(AF_INET, SOCK_STREAM, 0);
	if(fd < 0) {
		perror("socket fd");
		exit(1);
	}
	return fd;
}

void init_sockAddr(int fd, uint16_t port)
{
	struct sockaddr_in servAddr;
	struct sockaddr_in *serv = &servAddr; 
	memset(serv, 0, sizeof(*serv));
	serv->sin_family = AF_INET;
	serv->sin_port = htons(port);
	serv->sin_addr.s_addr = htonl(INADDR_ANY);
	if ( bind( fd, (struct sockaddr *) serv, sizeof(*serv)) < 0) {
		//perror("bind");
		fprintf(stderr, "%s Server binding error: Port %d already in use!\n", PGRM_NAME, MY_PORT_NUMBER);
		exit(1);
	}
	
	if(port == MY_PORT_NUMBER)
		printf("%s: Server running on port %d\n", PGRM_NAME, MY_PORT_NUMBER);
}

void get_hostInfo(struct sockaddr_in* clientAddr)
{	
	char hostName[HOST_NAME_MAX];
	char service[20];
	getnameinfo((const struct sockaddr * restrict) clientAddr,
	             sizeof *clientAddr, hostName, 
	             sizeof hostName, service, sizeof service, 0);
	             
	printf("Connection Established with: %s\n", hostName);
}

void send_ack(int fd, uint16_t *portNum)
{
	write(fd, "A", 1);
	
	if(portNum != NULL) {
		//printf("%d: New Data Port: %u\n", pid, *portNum);
		dprintf(fd, "%u", *portNum);
	}
	
	write(fd, "\n", 1);
}

void send_err(int fd, char *errMsg)
{
	write(fd, "E", 1);
	
	if(errMsg != NULL)
		write(fd, errMsg, strlen(errMsg));
	
	write(fd, "\n", 1);
}

void open_dataPort(int fd)
{
	datafd = init_socket();	
	init_sockAddr(datafd, 0);

	struct sockaddr_in dataAddr;
	unsigned int len = sizeof(struct sockaddr_in);
	memset(&dataAddr, 0, len);

	getsockname(datafd, (struct sockaddr * restrict) &dataAddr, (socklen_t * restrict) &len);
	dataPort = ntohs(dataAddr.sin_port);
	send_ack(fd, &dataPort);
	
	if(listen( datafd, 1 ) < 0)
			perror("data listen");

	struct sockaddr_in clientAddr;
	
	printf("%d: DATA Connection Waiting on port %d...\n", pid, dataPort);
	dataClientFD = accept(datafd, (struct sockaddr *) &clientAddr, &len);
	printf("%d: DATA Connection Established on port %d\n", pid, dataPort);
}

void server_IO(int fd, char *cmd, char *arg)
{
	if(dataClientFD > 0) {
		
		if(fork()) {
			close_dataPort();
			wait(NULL);
		}
		else {
			dup2(dataClientFD, STD_OUT);
			send_ack(fd, NULL);
			execlp(cmd, cmd, arg, (char *)NULL);
			
			perror("fork error");
			exit(0);
		}
		
	}
	else {
		fprintf(stderr, "%d Error: Data connection has not been established!\n", pid);
		send_err(fd, "DATA Connection not established!");
		close_dataPort();
	}
}

void process_command(int fd, char *cmd)
{
	if(cmd[0] == CMD_CD) {
		char *path = strtok(cmd, "C\n");
		printf("%d: RCD: %s\n", pid, path);
		if(chdir(path) < 0)
			send_err(fd, strerror(errno));
		else
			send_ack(fd, NULL);
	}
	else if(cmd[0] == CMD_LIST) {
		// I/O redirect ls -l | more
		printf("%d: CMD_LIST called!\n", pid);
		server_IO(fd, "ls", "-l");

	}
	else if(cmd[0] == CMD_DATA) {
		open_dataPort(fd);
	}
	else if(cmd[0] == CMD_GET) {
		char *path = strtok(cmd, "G\n");
		printf("%d: CMD_GET called!\n", pid);
		server_IO(fd, "cat", path);
		
	}
	else if(cmd[0] == CMD_PUT) {
		char *path = strtok(cmd, "P\n");
		printf("CMD_PUT called!\n");
		
		if(dataClientFD > 0) {
			if(file_exists(path)) {
				send_err(fd, "Error: File already exists.");
				close_dataPort();
			}
			else {
				send_ack(fd, NULL);
				IO_redir(dataClientFD, "cat", (char *)NULL, open_path(path));
			}
		}
		else {
			send_err(fd, "Data connection not established!");
			close_dataPort();
		}
	}
}

void read_command(int fd)
{
	pid = getpid();
	int readStat = 0;
	do {
		memset(msg, 0, MSG_BUF);
		char *byte = msg;
		
		while( (readStat = read(fd, byte++, 1)) > 0 ) {
			if( *(byte-1) == '\n')
				break;
		}
		
		if(readStat < 0) {
			perror("read_command");
			return;
		}
		else if(readStat == 0) {
			fprintf(stderr, "%d: Command connection lost/closed.\n", pid);
			return;
		}
		//printf("%d: Received msg: %s", pid, msg);
		
		//process command
		process_command(fd, msg);
		
	} while(msg[0] != CMD_QUIT);
	
	send_ack(fd, NULL);
}

void init_connection(int listenfd)
{
	listen( listenfd, MAX_PENDING );	
	int connectfd;
	unsigned int length = sizeof(struct sockaddr_in);
	struct sockaddr_in clientAddr;
			  
	while(1) {
		
		connectfd = accept(listenfd, (struct sockaddr *) &clientAddr, &length);
		if(fork()) {
			close(connectfd);
			waitpid(-1, NULL, WNOHANG);
		}
		else {
			get_hostInfo(&clientAddr);
			read_command(connectfd);
			close(connectfd);
			return;
		}
    
	}
	
}

int main(int argc, char **argv)
{
	int listenfd = init_socket();	
	init_sockAddr(listenfd, MY_PORT_NUMBER);
	init_connection(listenfd);
	
	return 0;
}
